// for normal searching
[
    {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},
]

//output

[
 

]




//number searching
.find({age:25})
.find({age:{$gt:25}})
.find({age:{$gte:25}})
.find({age:{$lt:25}})
.find({age:{$lte:25}})
.find({age:{$ne:25}})
.find({age:{$in:[20, 25, 30]}})
.find({age:{$gte:20, $lte:25}})


// string
.find({name:"nitan"})
.find({name:{$in:["nitan","ram"]}})
.find({name:"n"})






/* 
i)find({}) => find all object
ii)find({name:"nitan"}) => The object  object which has name nitan
iii)find({ name: "hari", age: 23 })=>find those object whose name is hari and age is 23
 		for number and boolean
iv)find({age:25}) 
and find({name:"25"})   are same  because while serching we focus on value not type
v)find({isMarried:false}) and find({isMarried:"false"})  are same
vi)greater than,  greater or equal to,  find({age:{$gt:18}} , find({age:{$gte:18}}
vii)equal to  not equal to find({age:{$ne:18}}
viii)less than, less than or equal to find({age:{$lt:18}} , find({age:{$lte:18}}
ix)include   find({name:{$in:["hari", "nitan"]}})   //it will give those object whose name includes either hari or nitan
x)include   find({name:{$nin:["hari", "nitan"]}})   //it will give those object whose name does not includes either hari or nitan
x)  $or and $and
    or  =>  find({$or:[{name:"nitan", age:30},{name:"nitan",age:40}]})  => get the object if the object matches both case case1 and case2
    and  => find($and: [{ age: { $gte: 18 } }, { age: { $lte: 25 } }])  => get the object only if it matches both case, case1 and case 2
    and  => find($and: [{ age: 18 }, { age: 29}])  => ask does it produce and output (no)

*/


//for  regex searchin
// [
//    {name:"ni1t",age:29, isMarried:false},
//     {name:"sand2inip",age:25, isMarried:false},
//     {name:"ni",age:26, isMarried:true},
//     {name:"ris3hav",age:20, isMarried:false},
//     {name:"nitan",age:29, isMarried:true},
//     {name:"chhimi",age:15, isMarried:true},
//     {name:"narendran",age:27, isMarried:false},
//     {name:"Nitan",age:16, isMarried:false},
//     {name:"nitanthapa",age:22, isMarried:false},
// ]


/* 
[
  {name:"nitan",age:29,},
{name:"nitanthapa",age:22},
  
]
*/

.find({name:/nitan/}).select("name age")






 

// find({name:"nitan"})//exact searching
// find({name:/nitan/})// regex searching => not exact searching
// find(name:/nitan/i)
// find(name:/ni/)
// find(name:/^ni/)
// find(name:/^ni/i)
// find(name:/n$/i)
// find(name:/^(?=.*[a-zA-Z])(?=.*\d).+/)

// find(name:/^(?=.*@)(?=.*_)|(?=.*_)(?=.*@)/)






  
  //find({name:"nitan"})
  // find({ "location.country": "nepal" });
  // find({favTeacher:"bhishma"})
  //find({"favSubject.bookAuthor":"nitan"})
  

  //find it determine which object to show or not
  //select
  //sort
  //limit
  //skip


  //for sorting
  [
    {name:"ac",age:29, isMarried:false},
    {name:"b",age:40, isMarried:false},
    {name:"ab",age:50, isMarried:false},
    {name:"ab",age:60, isMarried:false},

   {name:"c",age:40, isMarried:false},
   
  ]
  //output
  [
    {name:"ac",age:29, isMarried:false},
    {name:"c",age:40, isMarried:false},
    {name:"b",age:40, isMarried:false},
   {name:"ab",age:50, isMarried:false},
   {name:"ab",age:60, isMarried:false},
    
]



// .find({}).sort("name")
// find({}).sort("-name")
// find({}).sort("name age")
// find({}).sort("name -age")
// find({}).sort("-name age")
// find({}).sort("age -name")


//number sorting work properly unlike javascript
// find({}).sort("name")
// find({}).sort("-name")
// find({}).sort("name age")
// find({}).sort("name -age")

// //ascending sort  descending sort
// 

// find({}).sort("-name age")

// find({}).sort("age -name")


//skip
[
    {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},
]


.find({}).skip("3")




//output
[
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},

]

find({}).skip("8")



//limit
[
    {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},
]

//outptu
[
 

]

.find({}).limit("4")






//skip and limit
[
    {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},
]

//output
[

 
]

// find({}).limit("5").skip("2")
// find({}).skip("2").limit('5')

//this order works
//find , sort, select, skip, limit