import express, { json } from "express";
import { router1 } from "./src/route/firstRouter.js";
import { middleware } from "./src/route/traineesRouter.js";
import { traineesRouter } from "./src/route/1.js";
import { db } from "./src/database/DBconnect.js";
import { studentRouter } from "./src/route/studentRouter.js";
import { teacherRoute } from "./src/route/teacherRoute.js";
import { collegeRouter } from "./src/route/collegeRouter.js";
import { blogRouter } from "./src/route/blogRouter.js";
import { contactRouter } from "./src/route/contactRouter.js";
import { classroomRouter } from "./src/route/classroomRouter.js";
import { departmentRouter } from "./src/route/departmentRouter.js";
import { bookRouter } from "./src/route/bookRouter.js";
import { productRouter } from "./src/route/productRouter.js";
import { userRouter } from "./src/route/userRouter.js";
import { reviewRouter } from "./src/route/reviewRouter.js";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { config } from "dotenv";
import { port } from "./connect.js";
import { fileRouter } from "./src/route/fileRouter.js";

const app = express();
app.use(json());
app.use("/", router1);
app.use("/trainees", traineesRouter);
app.use("/api", middleware);
app.use("/student", studentRouter);
app.use("/teacher", teacherRoute);
app.use("/college", collegeRouter);
app.use("/blog", blogRouter);
app.use("/contact", contactRouter);
app.use("/classroom", classroomRouter);
app.use("/department", departmentRouter);
app.use("/book", bookRouter);
app.use("/product", productRouter);
app.use("/user", userRouter);
app.use("/review", reviewRouter);
app.use("/file", fileRouter);

app.use(express.static("./public/"));
config();

db();

app.listen(port, () => {
  console.log(`app listening on port: ${port}`);
});

// let password = "password";
// let hashedPassword = await bcrypt.hash(password, 10);

// console.log(hashedPassword);

// let hashedPassword= "$2b$10$.vg2KtXrneR17MEE8SxPv.exLG5tNQtCw6JdiU2.aKdqqT9oKS0/."

// let isValidPassword = await bcrypt.compare(password,hashedPassword)
// console.log(isValidPassword)

let object = {
  name: "Mahesh",
};
let key = "anything";
let expires = {
  expiresIn: "1d",
};

// let token = jwt.sign(object,key ,expires)
// console.log(token)

// let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiTWFoZXNoIiwiaWF0IjoxNzA2MzQxNjkzLCJleHAiOjE3MDY0MjgwOTN9.qT8dyS5TG7oJw3vtOzmTPvyUYcoCdV0a2CvHsBmUQg4"

// try {
//   let verifyToken = jwt.verify(token, key)
//   console.log(verifyToken)
// } catch (error) {
//   console.log(error.message)
// }
