import { Book } from "../schema/model.js"

export let createBook = async(req,res,next)=>{
    let data = req.body
    try{
        let result = await Book.create(data)
        res.json({
            sucess: true,
            message: "Book created successfully",
            result: result
        })
    }
    catch(error){
        res.json({
            sucess: false,
            message: error.message
        })

    }
}

export let readBook = async (req,res,next)=>{
    try{
        let result = await  Book.find({})
        res.json({
            sucess: true,
            message: "Book get successfully",
            data: result
        })

    }
    catch{
        res.json({
            sucess: false,
            message: "failed to get data",
        })
    }  
}

export let readBookById = async(req,res,next)=>{
    let id = req.params.id
    try{
        let result =  await Book.findById(id)
        res.json({
            sucess: true,
            message: "Book get successfully",
            result: result
        })
    }
    catch(error){
        res.json({
            sucess: false,
            message: error.message
        })

    }
}

export let updateBook = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body
    try {
        let result =await Book.findByIdAndUpdate(id,data,{new:true})
        res.json({
            sucess: true,
            message: "Book updated successfully",
            result: result
        })
    } catch (error) {
        res.json({
            sucess: false,
            message: error.message
        }) 
    }
}

export let deleteBook = async(req,res,next)=>{
    let id = req.params.id
    try {
        let result=await Book.findByIdAndDelete(id)
        if (result===null){
            res.json({
                success: false,
                message: "Book does not exist"
            })
        }
        else{
            res.json({
                sucess: true,
                message: "Book deleted successfully",
                result: result
            })
        }

    } catch (error) {
        res.json({
            sucess: false,
            message: error.message
        })
        
    }
    
}