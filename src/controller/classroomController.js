import { Classroom } from "../schema/model.js"

export let createClassroom = async(req,res,next)=>{
    let data = req.body
    try{
        let result = await Classroom.create(data)
        res.json({
            sucess: true,
            message: "Classroom created successfully",
            result: result
        })
    }
    catch(error){
        res.json({
            sucess: false,
            message: error.message
        })

    }
}

export let readClassroom = async (req,res,next)=>{
    try{
        let result = await  Classroom.find({})
        res.json({
            sucess: true,
            message: "Classroom get successfully",
            data: result
        })

    }
    catch{
        res.json({
            sucess: false,
            message: "failed to get data",
        })
    }  
}

export let readClassroomById = async(req,res,next)=>{
    let id = req.params.id
    try{
        let result =  await Classroom.findById(id)
        res.json({
            sucess: true,
            message: "Classroom get successfully",
            result: result
        })
    }
    catch(error){
        res.json({
            sucess: false,
            message: error.message
        })

    }
}

export let updateClassroom = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body
    try {
        let result =await Classroom.findByIdAndUpdate(id,data,{new:true})
        res.json({
            sucess: true,
            message: "Classroom updated successfully",
            result: result
        })
    } catch (error) {
        res.json({
            sucess: false,
            message: error.message
        }) 
    }
}

export let deleteClassroom = async(req,res,next)=>{
    let id = req.params.id
    try {
        let result=await Classroom.findByIdAndDelete(id)
        if (result===null){
            res.json({
                success: false,
                message: "Classroom does not exist"
            })
        }
        else{
            res.json({
                sucess: true,
                message: "Classroom deleted successfully",
                result: result
            })
        }

    } catch (error) {
        res.json({
            sucess: false,
            message: error.message
        })
        
    }
    
}