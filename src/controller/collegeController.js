import { College } from "../schema/model.js"

export let createCollege = async(req,res,next)=>{
    let data = req.body
    try{
        let result = await College.create(data)
        res.json({
            sucess: true,
            message: "College created successfully",
            result: result
        })
    }
    catch(error){
        res.json({
            sucess: false,
            message: error.message
        })

    }
}

export let readCollege = async (req,res,next)=>{
    try{
        let result = await  College.find({})
        res.json({
            sucess: true,
            message: "College get successfully",
            data: result
        })

    }
    catch{
        res.json({
            sucess: false,
            message: "failed to get data",
        })
    }  
}

export let readCollegeById = async(req,res,next)=>{
    let id = req.params.id
    try{
        let result =  await College.findById(id)
        res.json({
            sucess: true,
            message: "College get successfully",
            result: result
        })
    }
    catch(error){
        res.json({
            sucess: false,
            message: error.message
        })

    }
}

export let updateCollege = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body
    try {
        let result =await College.findByIdAndUpdate(id,data,{new:true})
        res.json({
            sucess: true,
            message: "College updated successfully",
            result: result
        })
    } catch (error) {
        res.json({
            sucess: false,
            message: error.message
        }) 
    }
}

export let deleteCollege = async(req,res,next)=>{
    let id = req.params.id
    try {
        let result=await College.findByIdAndDelete(id)
        if (result===null){
            res.json({
                success: false,
                message: "College does not exist"
            })
        }
        else{
            res.json({
                sucess: true,
                message: "College deleted successfully",
                result: result
            })
        }

    } catch (error) {
        res.json({
            sucess: false,
            message: error.message
        })
        
    }
    
}