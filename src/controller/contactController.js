import { Contact } from "../schema/model.js"

export let createContact = async(req,res,next)=>{
    let data = req.body
    try{
        let result = await Contact.create(data)
        res.json({
            sucess: true,
            message: "Contact created successfully",
            result: result
        })
    }
    catch(error){
        res.json({
            sucess: false,
            message: error.message
        })

    }
}

export let readContact = async (req,res,next)=>{
    try{
        let limit = req.query.limit
        let page = req.query.page

        let result = await  Contact.find({}).skip((page*limit)-limit).limit(limit)

        // let result = await  Contact.find({})
        res.json({
            sucess: true,
            message: "Contact get successfully",
            data: result
        })

    }
    catch{
        res.json({
            sucess: false,
            message: "failed to get data",
        })
    }  
}

export let readContactById = async(req,res,next)=>{
    let id = req.params.id
    try{
        let result =  await Contact.findById(id)
        res.json({
            sucess: true,
            message: "Contact get successfully",
            result: result
        })
    }
    catch(error){
        res.json({
            sucess: false,
            message: error.message
        })

    }
}

export let updateContact = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body
    try {
        let result =await Contact.findByIdAndUpdate(id,data,{new:true})
        res.json({
            sucess: true,
            message: "Contact updated successfully",
            result: result
        })
    } catch (error) {
        res.json({
            sucess: false,
            message: error.message
        }) 
    }
}

export let deleteContact = async(req,res,next)=>{
    let id = req.params.id
    try {
        let result=await Contact.findByIdAndDelete(id)
        if (result===null){
            res.json({
                success: false,
                message: "Contact does not exist"
            })
        }
        else{
            res.json({
                sucess: true,
                message: "Contact deleted successfully",
                result: result
            })
        }

    } catch (error) {
        res.json({
            sucess: false,
            message: error.message
        })
        
    }
    
}