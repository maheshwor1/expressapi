import { Department } from "../schema/model.js"

export let createDepartment = async(req,res,next)=>{
    let data = req.body
    try{
        let result = await Department.create(data)
        res.json({
            sucess: true,
            message: "Department created successfully",
            result: result
        })
    }
    catch(error){
        res.json({
            sucess: false,
            message: error.message
        })

    }
}

export let readDepartment = async (req,res,next)=>{
    try{
        let result = await  Department.find({})
        res.json({
            sucess: true,
            message: "Department get successfully",
            data: result
        })

    }
    catch{
        res.json({
            sucess: false,
            message: "failed to get data",
        })
    }  
}

export let readDepartmentById = async(req,res,next)=>{
    let id = req.params.id
    try{
        let result =  await Department.findById(id)
        res.json({
            sucess: true,
            message: "Department get successfully",
            result: result
        })
    }
    catch(error){
        res.json({
            sucess: false,
            message: error.message
        })

    }
}

export let updateDepartment = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body
    try {
        let result =await Department.findByIdAndUpdate(id,data,{new:true})
        res.json({
            sucess: true,
            message: "Department updated successfully",
            result: result
        })
    } catch (error) {
        res.json({
            sucess: false,
            message: error.message
        }) 
    }
}

export let deleteDepartment = async(req,res,next)=>{
    let id = req.params.id
    try {
        let result=await Department.findByIdAndDelete(id)
        if (result===null){
            res.json({
                success: false,
                message: "Department does not exist"
            })
        }
        else{
            res.json({
                sucess: true,
                message: "Department deleted successfully",
                result: result
            })
        }

    } catch (error) {
        res.json({
            sucess: false,
            message: error.message
        })
        
    }
    
}