let baseUrl = "http://localhost:8001/";
export let file = (req, res, next) => {
  let data = req.file;
  console.log(data);
  try {
    res.json({
      success: true,
      message: "file uploaded successfully",
      link: baseUrl + data.filename,
    });
  } catch (error) {}
};

export let fileMultiple = (req, res, next) => {
  let data = req.file.map((value, i) => {
    return baseUrl + value.filename;
  });
  console.log(data);
  try {
    res.json({
      success: true,
      message: "file uploaded successfully",
      link: data,
    });
  } catch (error) {}
};
