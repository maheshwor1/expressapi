import { Review } from "../schema/model.js"

export let createReview = async(req,res,next)=>{
    let data = req.body
    try{
        let result = await Review.create(data)
        res.json({
            sucess: true,
            message: "Review created successfully",
            result: result
        })
    }
    catch(error){
        res.json({
            sucess: false,
            message: error.message
        })

    }
}

export let readReview = async (req,res,next)=>{
    try{
        let limit = req.query.limit
        let page = req.query.page

        let result = await  Review.find({}).populate("productID").populate("userID")

        // let result = await  Review.find({})
        res.json({
            sucess: true,
            message: "Review get successfully",
            data: result
        })

    }
    catch{
        res.json({
            sucess: false,
            message: "failed to get data",
        })
    }  
}

export let readReviewById = async(req,res,next)=>{
    let id = req.params.id
    try{
        let result =  await Review.findById(id)
        res.json({
            sucess: true,
            message: "Review get successfully",
            result: result
        })
    }
    catch(error){
        res.json({
            sucess: false,
            message: error.message
        })

    }
}

export let updateReview = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body
    try {
        let result =await Review.findByIdAndUpdate(id,data,{new:true})
        res.json({
            sucess: true,
            message: "Review updated successfully",
            result: result
        })
    } catch (error) {
        res.json({
            sucess: false,
            message: error.message
        }) 
    }
}

export let deleteReview = async(req,res,next)=>{
    let id = req.params.id
    try {
        let result=await Review.findByIdAndDelete(id)
        if (result===null){
            res.json({
                success: false,
                message: "Review does not exist"
            })
        }
        else{
            res.json({
                sucess: true,
                message: "Review deleted successfully",
                result: result
            })
        }

    } catch (error) {
        res.json({
            sucess: false,
            message: error.message
        })
        
    }
    
}