import { Student } from "../schema/model.js"

export let createStudent = async(req,res,next)=>{
    let data = req.body
    try{
        let result = await Student.create(data)
        res.json({
            sucess: true,
            message: "student created successfully",
            result: result
        })
    }
    catch(error){
        res.json({
            sucess: false,
            message: error.message
        })

    }
}

export let readStudent = async (req,res,next)=>{
    try{
        // let result = await  Student.find({roll: {$gt: 10}})
        // let result = await  Student.find({roll: {$lt: 10}})
        // let result = await  Student.find({roll: {gte: 10}})
        // let result = await  Student.find({roll: {$in: [10,20,30]}})
        // let result = await  Student.find({roll: {$gte: 10, $lte: 20}})

        // let result = await  Student.find({}).select("name gender -_id")--to fetch selected data form database


        let limit = req.query.limit
        let page = req.query.page

        let result = await  Student.find({}).skip(page).limit(limit)
        res.json({
            sucess: true,
            message: "student get successfully",
            data: result
        })

    }
    catch{
        res.json({
            sucess: false,
            message: "failed to get data",
        })
    }  
}

export let readStudentById = async(req,res,next)=>{
    let id = req.params.id
    try{
        let result =  await Student.findById(id)
        res.json({
            sucess: true,
            message: "student get successfully",
            result: result
        })
    }
    catch(error){
        res.json({
            sucess: false,
            message: error.message
        })

    }
}

export let updateStudent = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body
    try {
        let result =await Student.findByIdAndUpdate(id,data,{new:true})
        res.json({
            sucess: true,
            message: "student updated successfully",
            result: result
        })
    } catch (error) {
        res.json({
            sucess: false,
            message: error.message
        }) 
    }
}

export let deleteStudent = async(req,res,next)=>{
    let id = req.params.id
    try {
        let result=await Student.findByIdAndDelete(id)
        if (result===null){
            res.json({
                success: false,
                message: "Student does not exist"
            })
        }
        else{
            res.json({
                sucess: true,
                message: "Student deleted successfully",
                result: result
            })
        }

    } catch (error) {
        res.json({
            sucess: false,
            message: error.message
        })
        
    }
    
}


// regex searching
// let result = await Stuednt.find({name:"nitan"})
// let result = await Stuednt.find({name:/nitan/})
// let result = await Stuednt.find({name:/nitan/i})
// let result = await Stuednt.find({name:/ni/})
// let result = await Stuednt.find({name:/ni/i})
// let result = await Stuednt.find({name:/^ni/})
// let result = await Stuednt.find({name:/^ni/i})
// let result = await Stuednt.find({name:/ni$/})