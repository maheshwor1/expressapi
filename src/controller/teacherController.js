import { Teacher } from "../schema/model.js"

export let createTeacher = async(req,res,next)=>{
    let data = req.body
    try{
        let result = await Teacher.create(data)
        res.json({
            sucess: true,
            message: "Teacher created successfully",
            result: result
        })
    }
    catch(error){
        res.json({
            sucess: false,
            message: error.message
        })

    }
}

export let readTeacher = async (req,res,next)=>{
    try{
        let result = await  Teacher.find({})
        res.json({
            sucess: true,
            message: "Teacher get successfully",
            data: result
        })

    }
    catch{
        res.json({
            sucess: false,
            message: "failed to get data",
        })
    }  
}

export let readTeacherById = async(req,res,next)=>{
    let id = req.params.id
    try{
        let result =  await Teacher.findById(id)
        res.json({
            sucess: true,
            message: "Teacher get successfully",
            result: result
        })
    }
    catch(error){
        res.json({
            sucess: false,
            message: error.message
        })

    }
}

export let updateTeacher = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body
    try {
        let result =await Teacher.findByIdAndUpdate(id,data,{new:true})
        res.json({
            sucess: true,
            message: "Teacher updated successfully",
            result: result
        })
    } catch (error) {
        res.json({
            sucess: false,
            message: error.message
        }) 
    }
}

export let deleteTeacher = async(req,res,next)=>{
    let id = req.params.id
    try {
        let result=await Teacher.findByIdAndDelete(id)
        if (result===null){
            res.json({
                success: false,
                message: "Teacher does not exist"
            })
        }
        else{
            res.json({
                sucess: true,
                message: "Teacher deleted successfully",
                result: result
            })
        }

    } catch (error) {
        res.json({
            sucess: false,
            message: error.message
        })
        
    }
    
}