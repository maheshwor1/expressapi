import { User } from "../schema/model.js";
import bcrypt from "bcrypt";
import { sendEmail } from "../utils/sendMail.js";

export let createUser = async (req, res, next) => {
  let data = req.body;
  let hashedPassword = await bcrypt.hash(data.password, 10);
  data.password = hashedPassword;
  try {
    let result = await User.create(data);
    sendEmail({
      to: [data.email],
      subject: "successfully created",
      html: `
      <div>
      <p>send mail successful</p>
      </div>`
    })
    res.json({
      sucess: true,
      message: "User created successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      sucess: false,
      message: error.message,
    });
  }
};

export let readUser = async (req, res, next) => {
  try {
    let limit = req.query.limit;
    let page = req.query.page;

    let result = await User.find({})
      .skip(page * limit - limit)
      .limit(limit);

    // let result = await  User.find({})
    res.json({
      sucess: true,
      message: "User get successfully",
      data: result,
    });
  } catch {
    res.json({
      sucess: false,
      message: "failed to get data",
    });
  }
};

export let readUserById = async (req, res, next) => {
  let id = req.params.id;
  try {
    let result = await User.findById(id);
    res.json({
      sucess: true,
      message: "User get successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      sucess: false,
      message: error.message,
    });
  }
};

export let updateUser = async (req, res, next) => {
  let id = req.params.id;
  let data = req.body;
  try {
    let result = await User.findByIdAndUpdate(id, data, { new: true });
    res.json({
      sucess: true,
      message: "User updated successfully",
      result: result,
    });
  } catch (error) {
    res.json({
      sucess: false,
      message: error.message,
    });
  }
};

export let deleteUser = async (req, res, next) => {
  let id = req.params.id;
  try {
    let result = await User.findByIdAndDelete(id);
    if (result === null) {
      res.json({
        success: false,
        message: "User does not exist",
      });
    } else {
      res.json({
        sucess: true,
        message: "User deleted successfully",
        result: result,
      });
    }
  } catch (error) {
    res.json({
      sucess: false,
      message: error.message,
    });
  }
};
