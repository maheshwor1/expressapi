import mongoose from "mongoose"
import { mongo_url } from "../../connect.js"

export let db = ()=>{
    mongoose.connect(mongo_url)
}