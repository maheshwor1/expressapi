import { Router } from "express";
export let traineesRouter = Router()

traineesRouter.route("/")
.post((req,res)=>{
    console.log(req.query)
    res.json({success: true,
    message: "trainees created "})
})

.get((req,res)=>{
    res.json({
        success: true,
        message: "Trainees read succefully"
    })
})
.patch((req,res)=>{

    let data =  req.body
    
    res.json(data)
})
.delete((req,res)=>{
    res.json({
        success: true,
        message: "Trainees deleted succefully"
    })
})


traineesRouter.route("/:id")
.get((req,res,next)=>{
    console.log(req.params)
    res.json({success: true})
})