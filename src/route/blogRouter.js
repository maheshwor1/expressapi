import { Router } from "express";
import { Blog } from "../schema/model.js";

export let blogRouter = Router()

blogRouter.route("/")
.post(async(req,res,next)=>{
    let data = req.body
    try{
        let result = await Blog.create(data)
        res.json({
            sucess: true,
            message: "Blog created successfully",
            result: data
        })
    }
    catch(error){
        res.json({
            sucess: false,
            message: error.message
        })

    }
})    
.get(async (req,res,next)=>{
    try{
        let result = await  Blog.find({})
        res.json({
            sucess: true,
            message: "Blog get successfully",
            data: result
        })

    }
    catch{
        res.json({
            sucess: false,
            message: "failed to get data",
        })

    }
    
})
.patch((req,res,next)=>{
    res.json({
        sucess: true,
        message: "Blog updated successfully"
    })
})
.delete((req,res,next)=>{
    res.json({
        sucess: true,
        message: "Blog deleted successfully"
    })
})

blogRouter.route("/:id")
.get(async(req,res,next)=>{
    let id = req.params.id
    try{
        let result =  await Blog.findById(id)
        res.json({
            sucess: true,
            message: "Blog get successfully",
            result: result
        })
    }
    catch(error){
        res.json({
            sucess: false,
            message: error.message
        })

    }

    
})
.patch(async(req,res,next)=>{
    let id = req.params.id
    let data = req.body
    try {
        let result =await Blog.findByIdAndUpdate(id,data,{new:true})
        res.json({
            sucess: true,
            message: "Blog updated successfully",
            result: result
        })
    } catch (error) {
        res.json({
            sucess: false,
            message: error.message
        })
        
    }
    
})
.delete(async(req,res,next)=>{
    let id = req.params.id
    try {
        let result=await Blog.findByIdAndDelete(id)
        if (result===null){
            res.json({
                success: false,
                message: "Blog does not exist"
            })
        }
        else{
            res.json({
                sucess: true,
                message: "Blog deleted successfully",
                result: result
            })
        }
        

    } catch (error) {
        res.json({
            sucess: false,
            message: error.message
        })
        
    }
    
})