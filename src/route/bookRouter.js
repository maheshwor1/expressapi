import { Router } from "express";
import { createBook, deleteBook, readBook, readBookById, updateBook } from "../controller/bookController.js";


export let bookRouter = Router()

bookRouter.route("/")
.post(createBook)    
.get(readBook)


bookRouter.route("/:id")
.get(readBookById)
.patch(updateBook)
.delete(deleteBook)