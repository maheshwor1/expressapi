import { Router } from "express";
import { Classroom } from "../schema/model.js";
import { createClassroom, deleteClassroom, readClassroom, readClassroomById, updateClassroom } from "../controller/classroomController.js";


export let classroomRouter = Router()

classroomRouter.route("/")
.post(createClassroom)    
.get(readClassroom)


classroomRouter.route("/:id")
.get(readClassroomById)
.patch(updateClassroom)
.delete(deleteClassroom)