import { Router } from "express";
import { College } from "../schema/model.js";
import { createCollege, deleteCollege, readCollege, readCollegeById, updateCollege } from "../controller/collegeController.js";

export let collegeRouter = Router()

collegeRouter.route("/")
.post(createCollege)    
.get(readCollege)


collegeRouter.route("/:id")
.get(readCollegeById)
.patch(updateCollege)
.delete(deleteCollege)