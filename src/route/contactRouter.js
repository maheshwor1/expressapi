import { Router } from "express";
import { createContact, deleteContact, readContact, readContactById, updateContact } from "../controller/contactController.js";

export let contactRouter = Router()

contactRouter.route("/")
.post(createContact)
.get(readContact)

contactRouter.route("/:id")
.get(readContactById)
.patch(updateContact)
.delete(deleteContact)