import { Router } from "express";
import { Department } from "../schema/model.js";
import { createDepartment, deleteDepartment, readDepartment, readDepartmentById, updateDepartment } from "../controller/departmentController.js";


export let departmentRouter = Router()

departmentRouter.route("/")
.post(createDepartment)    
.get(readDepartment)


departmentRouter.route("/:id")
.get(readDepartmentById)
.patch(updateDepartment)
.delete(deleteDepartment)