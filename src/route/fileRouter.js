import { Router } from "express";
import { file, fileMultiple } from "../controller/fileController.js";
import upload from "../utils/fileUpload.js";

export let fileRouter = Router()

fileRouter.route("/")
.post(upload.single("file"),file)

fileRouter.route("/multiple")
.post(upload.array("file"),fileMultiple)