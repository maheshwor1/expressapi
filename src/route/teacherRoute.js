import { Router } from "express";
import { Teacher } from "../schema/model.js";
import { createTeacher, deleteTeacher, readTeacher, readTeacherById, updateTeacher } from "../controller/teacherController.js";


export let teacherRoute = Router()

teacherRoute.route("/")
.post(createTeacher)    
.get(readTeacher)


teacherRoute.route("/:id")
.get(readTeacherById)
.patch(updateTeacher)
.delete(deleteTeacher)