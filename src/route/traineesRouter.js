import { Router } from "express";
export let middleware = Router()

middleware.route("/middleware")
.post((req,res)=>{
    console.log(req.query)
    res.json({success: true,
    message: "trainees created "})
})

.get((req,res,next)=>{
    console.log("i am middleware 1")
    next("abc")
},
(err,req,res,next)=>{
    console.log("i am middleware error")
    next("abc")
},
(req,res,next)=>{
    console.log("i am middleware 2")
})
.patch((req,res)=>{

    let data =  req.body
    
    res.json(data)
})
.delete((req,res)=>{
    res.json({
        success: true,
        message: "Trainees deleted succefully"
    })
})


middleware.route("/:id")
.get((req,res,next)=>{
    console.log(req.params)
    res.json({success: true})
})