
import { Schema } from "mongoose";

export let blogSchema = Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    }
})