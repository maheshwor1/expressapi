import { Schema } from "mongoose";

export let bookSchema = Schema({
    name: {
        type: String,
        required: true
    },
    author: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true

    },
    isAvailable: {
        type: Boolean,
        required: true
    }
})