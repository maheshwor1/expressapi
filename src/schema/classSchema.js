import { Schema } from "mongoose";

export let classSchema = Schema({
    name: {
        type: String,
        requred: true
    },
    numberOfBench: {
        type: Number,
        required: true
    },

    hasTv: {
        type: Boolean,
        required: true
    }
})