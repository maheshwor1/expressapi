import { Schema } from "mongoose";

export let departmentSchema = Schema({
    name: {
        type: String,
        required: true
    },
    hod: {
        type: String,
        required: true
    },
    totalmember: {
        type: Number,
        required: true
    },
})