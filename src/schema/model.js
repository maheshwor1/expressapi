import { model } from "mongoose";
import { studentSchema } from "./studentSchema.js"
import { teacherSchema } from "./teacherSchema.js";
import { collegeSchema } from "./collegeSchema.js";
import { blogSchema } from "./blogSchema.js";
import { contactSchema } from "./contactSchema.js";
import { classSchema } from "./classSchema.js";
import { departmentSchema } from "./departmentSchema.js";
import { bookSchema } from "./bookSchema.js";
import { productSchema } from "./productSchema.js";
import { userSchema } from "./userSchema.js";
import { reviewSchema } from "./reviewSchema.js";


export let Student = model("student",studentSchema)
export let Teacher = model("teacher", teacherSchema)
export let College = model("college", collegeSchema)
export let Blog = model("blog", blogSchema)
export let Contact = model("Contact", contactSchema)
export let Classroom = model("Classroom", classSchema)
export let Department = model("Department", departmentSchema)
export let Book = model("Book", bookSchema)
export let Product = model("Product", productSchema)
export let User = model("User", userSchema)
export let Review = model("Review", reviewSchema)



