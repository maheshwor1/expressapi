
import { Schema } from "mongoose";

export let reviewSchema = Schema({
    productId: {
        type: Schema.ObjectId,
        ref: "Product",
        required: true
    },
    userId: {
        type: Schema.ObjectId,
        ref: "User",
        required: true
    },
    description: {
        type: String,
        required: true
    }
})