import { Schema } from "mongoose";

export let studentSchema = Schema({
    name: {
        type: String,
        required: [true,"Name is required"],
        // lowercase: true
        trim: true,
        validate: (value) => {
            let alphabet = /^[a-zA-Z]+$/.test(value)
            if(!alphabet){
                throw new Error("Name can be only alphabetic characters")
            }
        }

    },
    
    password: {
        type: String,
        required: [true,"Password is required"],
        validate: (value)=>{
            let isValidPass = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+~-]).{8,15}$/.test(value)
            if(!isValidPass){
                throw new Error("Invalid password")
            }
        }

    },
    phoneNumber: {
        type: Number,
        required: [true,"Phone number is required"],
        validate: (value)=>{
            let data = value.toString()
            if(data.length !== 10){
                throw new Error("Invalid phone number")
            }
        }

    },
    roll: {
        type: Number,
        required: [true,"Roll number is required"]

    },
    isMarried: {
        type: Boolean,
        required: [true,"isMarried is required"]

    },
    spouseName: {
        type: String,
        required: [true,"spouseName is required"]
    },
    email: {
        type: String,
        required: [true,"Email is required"],
        unique: true,
        validate: (value)=>{
            let isValidEmail = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/.test(value)
            if(!isValidEmail){
                throw new Error("Invalid email")
            }
        }
    },
    gender: {
        default: "male",
        type: String,
        required: [true,"Gender is required"],
    },
    dob: {
        type: Date,
        required: [true,"DOB is required"],
    },
    
    location: {
        country: {
            type: String,
            required: [true,"country is required"],
        },
        district: {
            type: String,
            required: [true,"City is required"],
        }
    },
    favTeacher: [
        {
            type: String,
        }
    ],
    favBook: [
        {
            bookName:{
                type: String,
                required: [true, "Book name is required"]
            },
            bookAuthor:{
                type: String,
                required: [true, "Book author is required"]
            }
        }
    ]

})