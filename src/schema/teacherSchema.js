import { Schema } from "mongoose";


export let teacherSchema = Schema({
    name: {
        type: String,
        required: true,

    },

    class: {
        type: String,
        required: true,

    },

    faculty: {
        type: String,
        required: true,

    },
})